
// DécLaration d'un let + ajout d'une fonction permettant de gerer l'ordre des couleurs avec le temps //

let changeState = (function () {
    let state = 0,
        lamps = ["Rouge", "Orange", "Vert"],
        lampsLength = lamps.length,
        order = [
            [4000, "Rouge"],
            [2000, "Orange"],
            [4000, "Vert"],
            [2000, "Rouge"]
        ],
        orderLength = order.length,
        lampIndex,
        orderIndex,
        sId;

    return function (stop) {
        if (stop) {
            clearTimeout(sId);
            return;
        }

        let lamp,
        lampDOM;

        for (lampIndex = 0; lampIndex < lampsLength; lampIndex += 1) {
            lamp = lamps[lampIndex];
            lampDOM = document.getElementById(lamp);
            if (order[state].indexOf(lamp) !== -1) {
                lampDOM.classList.add("lamp" + lamp);
            } else {
                lampDOM.classList.remove("lamp" + lamp);
            }
        }

        sId = setTimeout(changeState, order[state][0]);
        state += 1;
        if (state >= orderLength) {
            state = 0;
        }
    };
}());

document.querySelector("#trafficLight").addEventListener("click", (function () {
    let state = false;
    
    return function () {
        changeState(state);
        state = !state;
    };
}()), false);